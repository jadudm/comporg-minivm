add:
	python minias.py add.asm > prog.h
	gcc -o vm minivm.c

bigadd:
	python minias.py bigadd.asm > prog.h
	gcc -o vm minivm.c

asm:
	gcc -O0 -S -c minivm.c
