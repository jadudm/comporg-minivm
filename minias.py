import re, sys

infile = open(sys.argv[1], 'r')

print "int code[] = {"

for line in infile:    
    res = re.search("load (\d+)", line)
    if res:
        print "\t0x4%s," % res.group(1)
    res = re.search("add", line)
    if res:
        print "\t0x25,"

print "\t0x00\n};"
