#include <stdio.h>
#include "prog.h"

#define A 0
#define B 1
#define C 2

// int code[] = {0x43, 0x45, 0x20, 0x00};
int stack[] = {0x00, 0x00, 0x00};

void print_stack (int ptr) {
  int i;
  
  // printf("---\nptr [ %X ]\n\n", ptr);
  printf("\nA [ %04X ]\n", stack[A]);
  printf("B [ %04X ]\n", stack[B]);
  printf("C [ %04X ]\n\n", stack[C]);
  
}
    
int main()
{
    int ptr = 0;
    int done = 0;
    
    while (!done) {
        int upper = code[ptr] >> 4;
        int lower = code[ptr] & 0x0F;
            
        switch (upper) {
            case 0x04: // Load Constant
            	stack[C] = stack[B];
            	stack[B] = stack[A];
            	stack[A] = lower;
            	break;
            case 0x02: // Add
              stack[A] = stack[A] + stack[B];
              break;
            case 0x00: // End
              print_stack(ptr);
              done = 1;
              break;
        }
        
        // Show the stack
        // print_stack(ptr);
        
        // Increment the code pointer.
   		ptr++;  	
    }
}
